#encoding: UTF-8
class ParseUserValidator < ActiveModel::Validator
  def validate(record)
    @user = User.where(:email => record.email)
    if @user.length > 0
      record.errors[:email] << "e-mail já registrado."
    end
  end
end